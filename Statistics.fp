s��         $     5  �   �     '�                                           On the fly Statistics                         � � ��Sinner     � ��Sinner*  � � ��SIO     � ��SIO*     � ��CStat     � ��CStat*     � ��CStatXY     � ��CStatXY*   D    Function panel for single and dual variable on-the-fly statistics.    �    Class of functions for single variable statistics.
Their first argument is always a pointer to a CStat typedef.

typedef struct CStat {
    unsigned long N;     // Number of values
    SInner S,S2;         // Sum of values and sum of squares
    SInner Min, Max;     // Extrema
} CStat;

Example of use:
CStat S;
StatReset(&S);
StatAdd(&S, 10);
StatAdd(&S, 0);
StatAdd(&S, 50);
A=StatAvr(&S);   // will return 20
// S.N = 3
// S.Min=0
// S.Max=50    �    Class of functions for single variable statistics.
Their first argument is always a pointer to a CStat typedef.

typedef struct CStatXY {
    unsigned long N;               // Number of values
    SInner Sx, Sy, Sxx, Syy, Sxy;  // Sum of values and sum of squares
    SInner MinX, MaxX, MinY, MaxY; // Extrema                          
    SInner YatminX, YatmaxX, XatminY, XatmaxY; // Values corresponding to extrema
} CStatXY;

Example of use:
CStatXY S;
StatXYReset(&S);
StatXYAdd(&S, 10, 20);
StatXYAdd(&S, 0, 10);
StatXYAdd(&S, 50, 0);
A=StatXYAvr(&S);   // will return 20
// S.N = 3
// S.MinX=0, S.YatminX=10
// S.Max=50, S.YatmaxX=0     @    Reset a Stat struct.
Should be called before starting to work.         Stat struct to reset    � $   �       Stat                               	            %    Add a data point to a Stat struct.
         Stat struct to add a point to     -    Point to add (coefficiant assumed to be 1).     $   �       Stat                              � $ � �       X                                  	            0    .    Add a weighted data point to a Stat struct.
         Stat struct to add a point to         Value of the point to add.     �    Coefficiant of the point to add.
Example:
    StatAddC(Stat, 10, 3)
is equivalent to calling
    StatAdd(Stat, 10)
3 times in a row
    � $   �       Stat                              � $ � �       X                                 � $,         Coef                               	            0    1    u    Remove a data point from a Stat struct.

Calling
StatAdd(Stat, 10)
StatSub(Stat, 10)
is equivalent to doing nothing         Stat struct to add a point to     0    Point to remove (coefficiant assumed to be 1).    	� $   �       Stat                              	� $ � �       X                                  	            0    %    Return the average of a Stat struct         Stat struct to operate on     ,    Average value contained in the Stat struct    
� $   �       Stat                              
� t����       Average                                	           &    Return the variance of a Stat struct         Stat struct to operate on     -    Variance value contained in the Stat struct    � $   �       Stat                              � t����       Variance                               	           0    Return the Standard deviation of a Stat struct         Stat struct to operate on     *    Sigma value contained in the Stat struct    � $   �       Stat                              � t����       Sigma                                  	           @    Reset a Stat struct.
Should be called before starting to work.         Stat struct to reset    � $   �       Stat                               	            %    Add a data point to a Stat struct.
         Stat struct to add a point to     -    Point to add (coefficiant assumed to be 1).     -    Point to add (coefficiant assumed to be 1).    J $   �       Stat                              q $ � �       X                                 � $/ �       Y                                  	            0    0    .    Add a weighted data point to a Stat struct.
         Stat struct to add a point to         Value of the point to add.     �    Coefficiant of the point to add.
Example:
    StatAddC(Stat, 10, 3)
is equivalent to calling
    StatAdd(Stat, 10)
3 times in a row
         Value of the point to add.    � $   �       Stat                              � $ � �       X                                  $�         Coef                              � $" �       Y                                  	            0    1    0    u    Remove a data point from a Stat struct.

Calling
StatAdd(Stat, 10)
StatSub(Stat, 10)
is equivalent to doing nothing         Stat struct to add a point to     0    Point to remove (coefficiant assumed to be 1).     0    Point to remove (coefficiant assumed to be 1).    / $   �       Stat                              V $ � �       X                                 � #N �       Y                                  	            0    0    %    Return the average of a Stat struct         Stat struct to operate on     ,    Average value contained in the Stat struct    � $   �       Stat                              � t����       Average                                	           %    Return the average of a Stat struct         Stat struct to operate on     ,    Average value contained in the Stat struct    � $   �       Stat                              � t����       Average                                	           &    Return the variance of a Stat struct         Stat struct to operate on     -    Variance value contained in the Stat struct    � $   �       Stat                              � t����       Variance                               	           &    Return the variance of a Stat struct         Stat struct to operate on     -    Variance value contained in the Stat struct    � $   �       Stat                              � t����       Variance                               	           0    Return the Standard deviation of a Stat struct         Stat struct to operate on     *    Sigma value contained in the Stat struct    � $   �       Stat                              � t����       Sigma                                  	           0    Return the Standard deviation of a Stat struct         Stat struct to operate on     *    Sigma value contained in the Stat struct    � $   �       Stat                              � t����       Sigma                                  	           %    Return the origin of the regression         Stat struct to operate on         Origin of the regression coef    � $   �       Stat                              � t����       Areg                                   	           $    Return the slope of the regression         Stat struct to operate on         Slope of the regression coef    � $   �       Stat                              � t����       Breg                                   	           $    Return the slope of the regression         Stat struct to operate on         Correlation coefficiant    � $   �       Stat                              � t����       Creg                                   	           C    Evaluate the value of Y for a given X.
If X=0, equivalent to Areg         Stat struct to add a point to         Point to evaluate    � $   �       Stat                              � $ � �       X                                      0    )    Evaluate the value of X for a given Y.
         Stat struct to add a point to         Point to evaluate    � $   �       Stat                              � $ � �       Y                                      0 ����         �       K.    StatReset                       ����         R  �     K.    StatAdd                         ����         V  e     K.    StatAddC                        ����         	  	�     K.    StatSub                         ����         
q  
�     K.    StatAvr                         ����         o  �     K.    StatVariance                    ����         o  �     K.    StatSigma                       ����         v  �     K.    StatXYReset                     ����           �     K.    StatXYAdd                       ����         �  �     K.    StatXYAddC                      ����         �  �     K.    StatXYSub                       ����         {  �     K.    StatXYAvrX                      ����         y  �     K.    StatXYAvrY                      ����         w  �     K.    StatXYVarX                      ����         w  �     K.    StatXYVarY                      ����         w       K.    StatXYSigmaX                    ����         ~       K.    StatXYSigmaY                    ����         �  �     K.    StatXYAreg                      ����         v  �     K.    StatXYBreg                      ����         e  �     K.    StatXYCreg                      ����         O  �     K.    StatXYEvalY                     ����         O  �     K.    StatXYEvalX                                                               ZSingle variable                      DReset Statistics                     DAdd to Statistics                    DAdd with coef to Statistics          DRemove from Statistics               DAverage                              DVariance                             DSigma                               "Dual variable                        DReset Statistics                     DAdd to Statistics                    DAdd with coef to Statistics          DRemove from Statistics               DAverage of X                         DAverage of Y                         DVariance of X                        DVariance of Y                        DSigma of X                           DSigma of Y                           DRegression origin                    DRegression slope                     DRegression correlation               DEvaluate Y for X                     DEvaluate X for Y                      toolbox.fp 